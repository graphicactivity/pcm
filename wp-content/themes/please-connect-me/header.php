<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Please_Connect_Me
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="https://use.typekit.net/pgv3cvi.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	
	<header id="masthead" class="site-header" role="banner">
		<ul class="headSplit">
			<li>
				<nav id="site-navigation" class="main-navigation" role="navigation">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php _e( '', 'please-connect-me' ); ?></button><span class="prompt">Menu</span>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
				</nav>
			</li>
			<li>
				<div id="homeButt" class="applyNow butt"><a href="#apply" rel="m_PageScroll2id">Apply Now</a></div>
				<div id="agentButt" class="applyNow butt"><a href="#apply" rel="m_PageScroll2id">Connect My Client</a></div>
			</li>
			<div class="branding"><a href="/"><img src="/wp-content/themes/please-connect-me/img/please-connect-me-wordmark.svg" width="200" alt="Please Connect Me" title="Please Connect Me"></a></div>
		</ul>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
		<div class="scroll"><div class="circ"><a href="#down" rel='m_PageScroll2id'></a></div><p>Scroll to find out more</p></div>
			<?php echo get_new_royalslider(1); ?>
