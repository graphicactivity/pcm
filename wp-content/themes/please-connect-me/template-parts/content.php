<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Please_Connect_Me
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="singleFeat">
		<?php the_post_thumbnail( 'post-thumbnails' ); ?>
		</div>

		<div class="archiveHold">
			<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php please_connect_me_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php
			endif; ?>

			<div class="entry-content">
				<?php
					the_content( sprintf(
						/* translators: %s: Name of current post. */
						wp_kses( __( 'Read More %s', 'please-connect-me' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'please-connect-me' ),
						'after'  => '</div>',
					) );
				?>
			</div>
		</div><!-- .entry-content -->

	</header><!-- .entry-header -->

</article><!-- #post-## -->
