<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Please_Connect_Me
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<div id="footer-sidebar1">
	        <?php
	        if(is_active_sidebar('footer-sidebar-1')){
	        dynamic_sidebar('footer-sidebar-1');
	        }
	        ?>
	        </div>
	        <div id="footer-sidebar2">
	        <?php
	        if(is_active_sidebar('footer-sidebar-2')){
	        dynamic_sidebar('footer-sidebar-2');
	        }
	        ?>
	        </div>
	        <div id="footer-sidebar3">
	        <?php
	        if(is_active_sidebar('footer-sidebar-3')){
	        dynamic_sidebar('footer-sidebar-3');
	        }
	        ?>
	        </div>

	        <ul class="socialLine">
	        	<li><a href="https://facebook.com/pleaseconnectme" target="_blank"><img src="/wp-content/themes/please-connect-me/img/ft-facebook.svg" alt="Facebook" title="Facebook" width="28" height="28"></a></li>
	        	<li><a href="https://linkedin.com/company/12893170" target="_blank"><img src="/wp-content/themes/please-connect-me/img/ft-linkedin.svg" alt="Linkedin" title="Linkedin" width="28" height="28"></a></li>
	        </ul>
	        <p>&copy;Please Connect Me <?php echo date("Y"); ?>&nbsp;|&nbsp;<a href="/terms-conditions">Terms &amp; Conditions</a></p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src='/wp-content/themes/please-connect-me/js/jquery.customSelect.js'></script>
<script>
	$(document).ready(function(){
    	$('.select-one').customSelect();
    });
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-87770860-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
