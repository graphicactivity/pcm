<?php
/**
 * Please Connect Me functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Please_Connect_Me
 */

if ( ! function_exists( 'please_connect_me_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function please_connect_me_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Please Connect Me, use a find and replace
	 * to change 'please-connect-me' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'please-connect-me', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'please-connect-me' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'please_connect_me_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'please_connect_me_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function please_connect_widgets_init() {
	register_sidebar( array(
		'name' => 'Footer Sidebar 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 3',
		'id' => 'footer-sidebar-3',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'please_connect_widgets_init' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function please_connect_me_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'please_connect_me_content_width', 640 );
}
add_action( 'after_setup_theme', 'please_connect_me_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function please_connect_me_scripts() {
	wp_enqueue_style( 'please-connect-me-style', get_stylesheet_uri() );

	wp_enqueue_script( 'please-connect-me-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
}
add_action( 'wp_enqueue_scripts', 'please_connect_me_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

function my_favicon() { ?>
<link rel="shortcut icon" href="/favicon.ico" >
<?php }
add_action('wp_head', 'my_favicon');

// custom admin login logo
function custom_login_logo() {
	echo '<style type="text/css">
	h1 a { background-image: url('.get_bloginfo('template_directory').'/img/logo.svg) !important; }
	</style>';
}
add_action('login_head', 'custom_login_logo');

function my_gallery_default_type_set_link( $settings ) {
    $settings['galleryDefaults']['link'] = 'file';
    return $settings;
}
add_filter( 'media_view_settings', 'my_gallery_default_type_set_link');

remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 12);

function intro_shortcode( $atts, $content = null ) {
	return '<section id="down" class="yourHome"><div class="inHome">' . do_shortcode($content) . '</div></section>';
}
add_shortcode( 'intro', 'intro_shortcode' );

function line_shortcode( $atts, $content = null ) {
	return '<div class="line">' . $content . '</div>';
}
add_shortcode( 'line', 'line_shortcode' );

function split_shortcode( $atts, $content = null ) {
	return '<section class="double"><ul class="split"><li>' . do_shortcode($content) . '</li></ul></section>';
}
add_shortcode( 'split', 'split_shortcode' );

function middle_shortcode( $atts, $content = null ) {
	return '</li>' . do_shortcode($content) . '<li>';
}
add_shortcode( 'middle', 'middle_shortcode' );

function save_shortcode( $atts, $content = null ) {
	return '<img src="/wp-content/themes/please-connect-me/img/save.svg" width="100" height="100" alt="Save Money" title="Save Money">' . $content . '';
}
add_shortcode( 'save', 'save_shortcode' );

function lefthover_shortcode( $atts, $content = null ) {
	return '<div id="connections" class="openView"><ul>' . do_shortcode($content) . '</ul></div>';
}
add_shortcode( 'lefthover', 'lefthover_shortcode' );

function connect_shortcode( $atts, $content = null ) {
	return '<img src="/wp-content/themes/please-connect-me/img/connections.svg" width="100" height="100" alt="Save Money" title="Save Money">' . $content . '';
}
add_shortcode( 'connect', 'connect_shortcode' );

function righthover_shortcode( $atts, $content = null ) {
	return '<div id="how" class="openView">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'righthover', 'righthover_shortcode' );

function getconnected_shortcode( $atts, $content = null ) {
	return '<div class="butt"><a href="#apply" rel="m_PageScroll2id">' . $content . '</a></div>';
}
add_shortcode( 'getconnected', 'getconnected_shortcode' );

function hoverli_shortcode( $atts, $content = null ) {
	return '<li>' . $content . '</li><br />';
}
add_shortcode( 'hoverli', 'hoverli_shortcode' );

function moving_shortcode( $atts, $content = null ) {
	return '<section class="moving"><div class="inMoving">' . do_shortcode($content) . '</div></section>';
}
add_shortcode( 'moving', 'moving_shortcode' );

function form_shortcode( $atts, $content = null ) {
	return '<section id="apply" class="getConnect">' . do_shortcode($content) . '</section>';
}
add_shortcode( 'form', 'form_shortcode' );

function formtext_shortcode( $atts, $content = null ) {
	return '<div class="inConnect">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'formtext', 'formtext_shortcode' );

function speak_shortcode( $atts, $content = null ) {
	return '<section class="speak"><div class="inSpeak">' . do_shortcode($content) . '</div></section>';
}
add_shortcode( 'speak', 'speak_shortcode' );

/* Agents page */

function referring_shortcode( $atts, $content = null ) {
	return '<section class="easy"><div class="inEasy">' . do_shortcode($content) . '</div></section>';
}
add_shortcode( 'referring', 'referring_shortcode' );

function actions_shortcode( $atts, $content = null ) {
	return '<ul class="process">' . do_shortcode($content) . '</ul>';
}
add_shortcode( 'actions', 'actions_shortcode' );

function service_shortcode( $atts, $content = null ) {
	return '<li>' . do_shortcode($content) . '</li>';
}
add_shortcode( 'service', 'service_shortcode' );

function brochure_shortcode( $atts, $content = null ) {
	return '<img src="/wp-content/themes/please-connect-me/img/brochure.svg" width="80" height="80" alt="Please Connect Me Brochure" title="Please Connect Me Brochure">' . $content . '';
}
add_shortcode( 'brochure', 'brochure_shortcode' );

function referral_shortcode( $atts, $content = null ) {
	return '<img src="/wp-content/themes/please-connect-me/img/referral.svg" width="80" height="80" alt="Please Connect Me Referral" title="Please Connect Me Referral">' . $content . '';
}
add_shortcode( 'referral', 'referral_shortcode' );

function application_shortcode( $atts, $content = null ) {
	return '<img src="/wp-content/themes/please-connect-me/img/application.svg" width="80" height="80" alt="Please Connect Me Application" title="Please Connect Me Application">' . $content . '';
}
add_shortcode( 'application', 'application_shortcode' );

function brochurehover_shortcode( $atts, $content = null ) {
	return '<div id="brochure" class="open">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'brochurehover', 'brochurehover_shortcode' );

function referralhover_shortcode( $atts, $content = null ) {
	return '<div id="brochure" class="open">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'referralhover', 'referralhover_shortcode' );

function applicationhover_shortcode( $atts, $content = null ) {
	return '<div id="application" class="open">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'applicationhover', 'applicationhover_shortcode' );

function infobutton_shortcode( $atts, $content = null ) {
	return '<div class="butt"><a href="mailto:info@pleaseconnectme.co.uk">' . $content . '</a></div>';
}
add_shortcode( 'infobutton', 'infobutton_shortcode' );

function emailbutton_shortcode( $atts, $content = null ) {
	return '<div class="butt"><a href="mailto:info@pleaseconnectme.co.uk">' . $content . '</a></div>';
}
add_shortcode( 'emailbutton', 'emailbutton_shortcode' );

function referralbutton_shortcode( $atts, $content = null ) {
	return '<div class="butt"><a href="#apply" rel="m_PageScroll2id">' . $content . '</a></div>';
}
add_shortcode( 'referralbutton', 'referralbutton_shortcode' );

function reffer_shortcode( $atts, $content = null ) {
	return '<section class="refer"><div class="inRefer">' . do_shortcode($content) . '</div></section>';
}
add_shortcode( 'reffer', 'reffer_shortcode' );

function underline_shortcode( $atts, $content = null ) {
	return '<ul class="reason">' . do_shortcode($content) . '</ul>';
}
add_shortcode( 'underline', 'underline_shortcode' );

function lines_shortcode( $atts, $content = null ) {
	return '<li>' . $content . '</li>';
}
add_shortcode( 'lines', 'lines_shortcode' );

function value_shortcode( $atts, $content = null ) {
	return '<section class="value"><div class="inValue">' . do_shortcode($content) . '</div></section>';
}
add_shortcode( 'value', 'value_shortcode' );