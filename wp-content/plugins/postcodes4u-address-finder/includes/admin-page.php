<?php

function pc4u_options_page() {

global $pc4u_options;
	ob_start(); ?>
	<div class="wrap">
		<h2>Postcodes4u Postcode Lookup Plugin Options</h2>
        <form method="post" action="options.php">

			<?php settings_fields('pc4u_settings_group');
            // Process Current/Default settings
            $currentSettings = get_option('pc4u_settings');
            $currentPc4uEnable = '0';

		    $currentPc4uUserName = '';

            $currentPc4uUserKey = '';
			$currentPc4uAltAddressDisp = '0';

 			$currentPc4uTestForm = '0';

            $currentWoocommerce = '0';
            $currentWoocommercePosition = '0';
            $currentWooForceTemplateOverride = '0';



            if(isset($currentSettings['enable']) ) {
                $currentPc4uEnable = $currentSettings['enable'] ;
            }
   			if(isset($currentSettings['user_name']) ) {
                $currentPc4uUserName = $currentSettings['user_name'] ;
            }

            if(isset($currentSettings['user_key']) ) {
                $currentPc4uUserKey = $currentSettings['user_key'] ;
            }

            // General Settings
			if(isset($currentSettings['alt_address_disp']) ) {
				// Only Allow If Postcode Integration Enabled
				if($currentPc4uEnable == '1') {
					$currentPc4uAltAddressDisp = $currentSettings['alt_address_disp'] ;
				}
			}

			if(isset($currentSettings['pc4u_testform']) ) {
				// Only Allow If Postcode Integration Enabled
				if($currentPc4uEnable == '1') {
					$currentPc4uTestForm = $currentSettings['pc4u_testform'] ;
				}
			}

			// WooCommerce Settings
            if(isset($currentSettings['woointegrate']) ) {
                // Only Allow If Postcodes4u Integration Enabled
                if($currentPc4uEnable == '1') {
                    $currentWoocommerce = $currentSettings['woointegrate'] ;
                }
            }
            if(isset($currentSettings['woocommerce_position']) ) {
				 // Only Allow If WooCommerce (And Pc4u) Integration Enabled
				if($currentWoocommerce == '1') {
					 $currentWoocommercePosition = $currentSettings['woocommerce_position'] ;
                }
            }
 			if(isset($currentSettings['woocommerce_templateoverride']) ) {
                 // Only Allow If WooCommerce (And Pc4u) Integration Enabled
                if($currentWoocommerce == '1') {
                    $currentWooForceTemplateOverride = $currentSettings['woocommerce_templateoverride'] ;
                }
            }
            ?>
	<h3>Enter your Postcodes4u key and Username to link to your Postcodes4u account.</h3>

	<h4>&nbsp;&nbsp;Login to your account at <a href="http://www.postcodes4u.co.uk"  target="_blank">Postcodes4u</a> for your account details.</h4>

	<p><strong>&nbsp;&nbsp;<?php _e('Postcodes4u Key', 'pc4u_domain'); ?></strong>
	 	<br>
		<input id="pc4u_settings[user_key]" name="pc4u_settings[user_key]" type="text" class="regular-text" value="<?php echo $currentPc4uUserKey; ?>"/>
		<label class="description" for="pc4u_settings[user_key]"><?php _e('Enter your Postcodes4u Key', 'pc4u_domain'); ?></label>
	</p>


	<p><strong>&nbsp;&nbsp;<?php _e('Postcodes4u Username', 'pc4u_domain'); ?></strong>
		<br>
		<input id="pc4u_settings[user_name]" name="pc4u_settings[user_name]" type="text" class="regular-text" value="<?php echo $currentPc4uUserName; ?>"/>
		<label class="description" for="pc4u_settings[user_name]"><?php _e('Enter your Postcodes4u Username', 'pc4u_domain'); ?></label>
		<br>
	</p>

	<h3><?php _e('Postcodes4u Active', 'pc4u_domain'); ?></h3>
	<p><strong>
		<input id="pc4u_settings[enable]" name="pc4u_settings[enable]" type="checkbox" value="1" <?php checked('1',$currentPc4uEnable); ?> />
		<label class="description" for="pc4u_settings[alt_address_disp]"><?php _e('Postcodes4u Postcode Lookups Enabled.', 'pc4u_domain'); ?></label>
		</strong>
	</p>
	<p>&nbsp;</p>

    <h3>Postcodes4u General Settings.</h3>


 	<p><strong><?php _e('Address Display Options', 'pc4u_domain'); ?></strong>
		<br>&nbsp;&nbsp;
		<input id="pc4u_settings[alt_address_disp]" name="pc4u_settings[alt_address_disp]" type="checkbox" value="1" <?php checked('1',$currentPc4uAltAddressDisp); ?> />
		<label class="description" for="pc4u_settings[alt_address_disp]"><?php _e('Use alternative address format - Ensure no duplicate fields.', 'pc4u_domain'); ?></label>
	</p>

	<p><strong><?php _e('Enable Test LookUp Form', 'pc4u_domain'); ?></strong>
		<br>&nbsp;&nbsp;
		<input id="pc4u_settings[pc4u_testform]" name="pc4u_settings[pc4u_testform]" type="checkbox" value="1" <?php checked('1',$currentPc4uTestForm); ?> />
		<label class="description" for="pc4u_settings[pc4u_testform]"><?php _e('Enable the Test Postcodes4u Lookup Form', 'pc4u_domain'); ?></label>
	</p>
	<p>&nbsp;</p>



	<h3>WooCommerce Integration Settings.</h3>
	<p><strong><?php _e('Enable Postcodes4u Integration in WooCommerce Checkout', 'pc4u_domain'); ?></strong>		<br>&nbsp;&nbsp;
		<input id="pc4u_settings[woointegrate]" name="pc4u_settings[woointegrate]" type="checkbox" value="1" <?php checked('1', $currentWoocommerce); ?> />		<label class="description" for="pc4u_settings[woointegrate]"><?php _e('Show the Postcodes4u Lookup in WooCommerce Checkout', 'pc4u_domain'); ?></label>	</p>	<p><strong><?php _e('   WooCommerce Postcode Lookup Display Position', 'pc4u_domain'); ?></strong>
		<br>&nbsp;&nbsp;
		<input name="pc4u_settings[woocommerce_position]" type="radio" value="0" <?php checked('0', $currentWoocommercePosition); ?> />
		<label class="description" for="pc4u_settings[woocommerce_position]"><?php _e('  At Top of Billing/Shipping Address (Default)', 'pc4u_domain'); ?></label>
		<br>&nbsp;&nbsp;
		<input name="pc4u_settings[woocommerce_position]" type="radio" value="1" <?php checked('1', $currentWoocommercePosition); ?> />
		<label class="description" for="pc4u_settings[woocommerce_position]"><?php _e('  Before Company Name', 'pc4u_domain'); ?></label>
		<br>&nbsp;&nbsp;
		<input name="pc4u_settings[woocommerce_position]" type="radio" value="2" <?php checked('2', $currentWoocommercePosition); ?> />
		<label class="description" for="pc4u_settings[woocommerce_position]"><?php _e('  Before First Line Of Address', 'pc4u_domain'); ?></label>
	</p>
	<p><strong><?php _e('Override Child Theme WooCommerce Checkout Templates', 'pc4u_domain'); ?></strong>
		<br>&nbsp;&nbsp;
		<input id="pc4u_settings[woocommerce_templateoverride]" name="pc4u_settings[woocommerce_templateoverride]" type="checkbox" value="1" <?php checked('1', $currentWooForceTemplateOverride); ?> />
		<label class="description" for="pc4u_settings[woocommerce_templateoverride]"><?php _e('Override Theme WooCommerce Checkout Templates. (See Note.)', 'pc4u_domain'); ?></label>
		<br><br>
		<strong>Note.</strong>
		Use this option if the current WordPress theme has it's own WooCommerce address templates, and you wish to use Postcode Lookup on the Billing and Shipping address pages.
		<br>
		&nbsp;&nbsp;Be aware that this may affect the styling of the Woocommerce address pages.
		<br>
	</p>
	<p class="submit">		<input type="submit" class="button-primary" value="<?php _e('Save Options', 'pc4u_domain'); ?>" />	</p>
	</form>
	</div>
	<?php
	echo ob_get_clean();
}

function pc4u_validate_settings( $pc4uInput ) {
    // If No User Details Then mark Postcodes4u as not enabled
    if( trim($pc4uInput['user_name']) === '' || trim($pc4uInput['user_key'] === '')) {
        // NO User Details - DISABLE POSTCODES 4u
         $pc4uInput['enable'] = '0';
         $pc4uInput['woointegrate'] = '0';
         add_settings_error( 'pc4u_settings',  esc_attr( 'settings_updated' ),
                                'You cannot enable the Postcodes4u Postcode Lookup without entering a Username and Key', 'pc4u_domain' );
     }
     return $pc4uInput;
}

function pc4u_add_options_link() {
	add_options_page('PC4U Plugin Options', 'Postcodes4U', 'manage_options', 'pc4u-options', 'pc4u_options_page');
}
add_action('admin_menu', 'pc4u_add_options_link');

function pc4u_register_settings() {

	register_setting('pc4u_settings_group', 'pc4u_settings', 'pc4u_validate_settings');
}
add_action('admin_init', 'pc4u_register_settings');

