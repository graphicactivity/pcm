<?php
/**
 * Checkout shipping information form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 *
 * Modified BY 3x SW For Postcodes 4u Lookup. 13/12/2016
 *  Added Additional Control Values for Postcode Lookup Placement
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;
global $pc4u_options;
?>

<div class="woocommerce-shipping-fields">
<?php if ( WC()->cart->needs_shipping() && ! WC()->cart->ship_to_billing_address_only() ) : ?>

	<?php
		if ( empty( $_POST ) ) {
			$ship_to_different_address = get_option( 'woocommerce_ship_to_billing' ) == 'no' ? 1 : 0;
			$ship_to_different_address = apply_filters( 'woocommerce_ship_to_different_address_checked', $ship_to_different_address );

		} else {

			$ship_to_different_address = $checkout->get_value( 'ship_to_different_address' );

		}
	?>

	<h3 id="ship-to-different-address">
		<label for="ship-to-different-address-checkbox" class="checkbox"><?php _e( 'Ship to a different address?', 'woocommerce' ); ?></label>
		<input id="ship-to-different-address-checkbox" class="input-checkbox" <?php checked( $ship_to_different_address, 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" />
	</h3>

	<div class="shipping_address">

		<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout );

		if($pc4u_options['woointegrate'] == true || $pc4u_options['woointegrate'] == '1') {
			// Do Special Postcode4u Lookup Stuff - Manually Add Postcode Field
            $pc4uShipDisplayed="no";

			// Set Postcodes4u Lookup Form Location
			$pc4uShipPosition="top";
			if($pc4u_options['woocommerce_position'] == '1') {
				$pc4uShipPosition='shipping_company';
            }
			if($pc4u_options['woocommerce_position'] == '2') {
				$pc4uShipPosition='shipping_address_1';
            }

			foreach ( $checkout->checkout_fields['shipping'] as $key => $field ) :
			   if($pc4uShipDisplayed=='no' && ($pc4uShipPosition=='top' || $key == $pc4uShipPosition)) {
			      ?>

			   	   <p class= "form-row form-row form-row-first">
			   	      <label for="shipping_postcode" class="">Shipping Postcode</label>
			   	      <input type="text" class="input-text " name="shipping_postcode" id="shipping_postcode" placeholder=""  value=""  />
			       </p>
			       <p class= "form-row form-row form-row-last">
			          <label class="">&nbsp;</label>
			          <input onclick="Pc4uWooSearchShippingBegin(); return false;" type="submit" value="Postcode Lookup" id="Pc4uShippingLookup" class = "Pc4uLookup" name="wooShipping" />
			       </p>

			   	      <select id="pc4uWooShippingDropdown"  class = "Pc4uDropdown" style="display: none;" onchange="Pc4uSearchIdBegin('pc4uWooShipping')"><option>Select an address:</option></select>
						<div class="clear">
				          <div id="postcodes4ukey" style="display: none;" ><?php echo $pc4u_options['user_key'];?></div>
				          <div id="postcodes4uuser" style="display: none;" ><?php echo $pc4u_options['user_name'];?> </div>
				          <div id="pc4ualt_address_disp" style="display: none;" ><?php echo $pc4u_options['alt_address_disp'];?> </div>
				       </div>
			       <?php
           	      $pc4uShipDisplayed='yes';
			   }

			   if( $key != "shipping_postcode" ) {
					woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
			   }
			endforeach;
		    ?>
			  <p class= "form-row form-row form-row-wide">&nbsp;</p>
			<?php

		} else {
		    // Normal Shipping Form
		    foreach ( $checkout->checkout_fields['shipping'] as $key => $field ) :
		        woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
		    endforeach;
		}
        ?>

		<?php do_action('woocommerce_after_checkout_shipping_form', $checkout); ?>

	</div>

   <?php endif; ?>

   <?php do_action('woocommerce_before_order_notes', $checkout); ?>

   <?php if (get_option('woocommerce_enable_order_comments')!='no') : ?>

   <?php if ( ! WC()->cart->needs_shipping() || WC()->cart->ship_to_billing_address_only() ) : ?>
	   <h3><?php _e( 'Additional Information', 'woocommerce' ); ?></h3>
   <?php endif; ?>


   <?php foreach ( $checkout->checkout_fields['order'] as $key => $field ) : ?>

        <?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

	<?php endforeach; ?>

 <?php endif; ?>

  <?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
</div>